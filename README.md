# Oefening

## Tentamens

In elk schooljaar worden meerdere tentamens afgenomen die op verschillende manieren worden beoordeeld.
Een bepaald aantal punten wordt bijgeteld voor een goed antwoord, afgetrokken voor een fout antwoord en voor een niet gegeven antwoord kunnen punten bijgeteld, afgetrokken of niet gegeven worden.

Geef het totaal aantal punten terug dat de scholier gescoord heeft op het tentamen.

De parameters van de score functie zijn:

- een array met 0, 1 en 2'en. 0 is een correct antwoord, 1 is een niet gegeven antwoord en 2 is een foutief antwoord.
- het aantal punten te behalen met een correct antwoord
- het aantal punten voor een niet gegeven antwoord (dit kan negatief zijn)
- het aantal punten voor een foutief antwoord

### Voorbeelden:

1:

[0, 0, 0, 0, 2, 1, 0], 2, 0, 1 --> 9

omdat:

&nbsp;&nbsp;&nbsp;&nbsp;5 correcte antwoorden: 5\*2 = 10

&nbsp;&nbsp;&nbsp;&nbsp;1 niet gegeven antwoorden: 1\*0 = 0

&nbsp;&nbsp;&nbsp;&nbsp;1 foutieve antwoorden: 1\*1 = 1

wordt: 10 + 0 - 1 = 9

2:

[0, 1, 0, 0, 2, 1, 0, 2, 2, 1], 3, -1, 2) --> 3

wordt: 4\*3 + 3\*-1 - 3\*2 = 3

#### Werkwijze

Fork deze repository, maak een feature branch aan.

Maak een src en test folder aan en maak de oefening af.
