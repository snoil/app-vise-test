const examCalculator = require('../src/index.js');

const test = examCalculator.calculateResult([0, 1, 0, 0, 2, 1, 0, 2, 2, 1], 3, -1, 2);
console.log(`Expect 3, got: ${test}`);
const test0 = examCalculator.calculateResult([0, 0, 0, 0, 2, 1, 0], 2, 0, 1);
console.log(`Expect 9, got: ${test0}`);

try {
    const test1 = examCalculator.calculateResult([3, 0, 0, 0, 0, 2, 1, 0], 2, 0, 1);
    console.log(`Expect 9, got: ${test1}`);
} catch (error) {
    console.error(error);
}


