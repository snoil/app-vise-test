module.exports = {
    // Calculates total points of exam based on given value of answer type.
    calculateResult : (
        answers: number[], 
        correctPoints: number, 
        emptyPoints: number, 
        incorrectPoints: number): number => {
            let result: number = 0;
            answers.forEach(answer => {
                switch (answer) {
                    case 0:
                        result += correctPoints;
                        break;
                    case 1:
                        result += emptyPoints;
                        break;
                    case 2:
                        result -= incorrectPoints;
                        break;
                    default:
                        throw 'Unvalid input';
                }
                
            });
        return result;
    }
}